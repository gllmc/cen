---
layout: post
title:  "Une compréhension inégale de la plateforme : causes et conséquences"
date:   2020-10-17 12:00:00 +0100
---
Salut tout le monde ! J’ai les résultats de l’enquête que j'ai menée. Merci d’avoir été si nombreux·ses à y répondre !

Les premiers résultats que je voudrais présenter et qui me semblent importants concernent la compréhension de Parcoursup. Un résultat que je trouve tout à fait impressionnant, c’est que seulement 22 % d’entre vous pensent comprendre le fonctionnement (y compris algorithmique) de Parcoursup ! J’espère que depuis, vous avez eu le temps de vous mettre à la page et de lire mes précédents posts ;)

# Quels facteurs peuvent impacter la compréhension de Parcoursup ?

Tout d'abord, sur l’ensemble total des personnes ayant répondu à mon questionnaire, on voit que le genre n’a pas d’impact sensible sur la compréhension de Parcoursup :

{:refdef: style="text-align: center;"}
![Pas d'impact du genre sur la compréhension]({{ "/assets/graphs/comprehension_selon_genre.png" | relative_url }})
{: refdef}

Je me suis dit qu’il pouvait être intéressant de comparer la compréhension de Parcoursup en fonction de la PCS (Professions et catégories socio-professionnelles) d’origine des parents. Cependant, étant donnée la difficulté du traitement des informations, je me suis dit que j’allais plutôt faire une typologie par le lycée d’origine. Selon une étude[^1] de 2004 menée entre 1989 et 1994 (oui, ça remonte mais je n’en ai pas trouvé une de plus récente), toutes choses égales par ailleurs, il y a un effet de secteur perceptible sur la réussite des élèves, notamment ceux issus des classes modestes ou avec une mère ne présentant pas ou peu de diplômes. Le privé sous contrat permet donc de réduire les écarts entre les groupes sociaux et a donc une fonction « démocratique ». On peut donc s’attendre à ce que ces élèves aient un léger avantage comparatif, mais que les résultats restent comparables avec ceux des élèves de lycée public hors REP, si notre échantillon est représentatif. La comparaison avec des lycéen·ne·s scolarisé·e·s en REP, dans des lycées socialement plus défavorisés, peut donc être un facteur intéressant et plus simple à analyser que les PCS brutes des parents. Ce qu’on voit, c’est que pour les lycées privés hors contrat et publics hors REP, les barres d’erreur ne se recoupent pas tout à fait, donc qu’un effet de secteur ne peut pas être exclu. Cependant, une différence tout à fait notable est à observer avec les élèves ayant fréquenté un lycée de REP, qui estiment pour 65 % d’entre elleux ne pas comprendre le fonctionnement de la plateforme. Le facteur social est donc très important.

# Expérience de la procédure

Un facteur ne pouvant être négligé est également l’expérience de la procédure. On remarque une différence entre les terminales n’ayant pas encore expérimenté la procédure, et les étudiant·e·s l’ayant déjà expérimenté :

{:refdef: style="text-align: center;"}
![Compréhension selon l'établissement]({{ "/assets/graphs/comprehension_selon_etablissement.png" | relative_url }})
![Compréhension selon le lycée]({{ "/assets/graphs/comprehension_selon_lycee.png" | relative_url }})
{: refdef}

La proportion de terminales n’ayant pas l’impression de comprendre Parcoursup est très similaire à la proportion de celleux ayant expérimenté la procédure. Cependant, on remarque un changement dans les résultats de celleux originaires de REP. Si celleux-ci pensent toustes avoir une compréhension moyenne de Parcoursup avant de passer par la procédure, celleux étant originaires de ces lycées sont, après avoir expérimenté la procédure, plutôt convaincu·e·s de ne pas du tout la comprendre. Pour les lycéens du public et du privé sous contrat, le fait d’avoir déjà expérimenté la procédure n’a qu’une influence minime. On observe en terminale une différence nette du sentiment de compréhension de la procédure entre ceux issus de lycées publics et privés sus contrat et ceux issus de REP. Selon un article[^2] publié en 2020, un autre biais peut venir du fait que plus les élèves connaitraient la procédure, plus elle leur paraitrait opaque et peu compréhensible (ce lien sera discuté plus tard). Ce résultat est cohérent avec le fait qu’après être passé·e·s Parcoursup, les lycéen·ne·s de REP ont moins la sensation de comprendre la procédure que leurs homologues en terminale. On a donc une tendance qui change après le passage par Parcoursup.

Le sentiment de compréhension de la procédure n’équivaut cependant pas à sa compréhension effective. Lors d’un entretien, j’ai demandé à A., scolarisée au lycée Galilée, un lycée public de banlieue peu côté comment elle pensait que Parcoursup fonctionnait.

> Ce que je sais, c’est qu’en milieu d’année tu dois présenter des vœux et sélectionner si tu veux une année de césure, présenter plusieurs écoles, et à la fin de l’année tu choisis selon là où tu es acceptée. […] Je crois qu’ils regardent ton dossier de première et terminale, les écoles elles choisissent comment elles classent mais je ne sais pas si ça dépend de là où tu habites. Il faut classer les vœux aussi je crois.

Elle pensait comprendre la procédure, tout en pensant que les vœux étaient encore hiérarchisés et que les facultés parisiennes étaient encore sectorisées (ce qui n’est plus le cas depuis 2019). Sur la sélection opérée par les formations, quand je lui demande si elle pense que son lycée d’origine a un impact sur son dossier, elle me répond :

> Oui, ça doit sûrement jouer, mais je ne sais pas.

Or, un article[^3] de 2016 montre que, pour les formations sélectives, le lycée d’origine est pris en compte. Les CPGE en particulier tiennent une liste des résultats effectifs aux concours et sur la réussite en CPGE en fonction du lycée d’origine, ce qui leur permet de permet de pondérer les résultats obtenus par les lycéens pour mieux les sélectionner. Le sentiment de compréhension n’équivaut donc pas à une compréhension effective de la procédure et du processus de sélection.

E., à la même question, m’a répondu que :

> Alors, quand c’est sorti j’en avais littéralement aucune idée, je comprenais rien du tout mais bon je m’en suis fait une petite idée : je pense que ça trie des élèves en fonction des notes, largement et euh c’est.. je pense que c’est un processus d'écrémage c’est tout. Enfin les lettres de motivation… Faut pas être stupide [rires] quand tu postules à une formation où t’as 150 places et 7000 personnes qui vont en envoyer une.. bon on s’en fout de la lettre. Je pense que c’est juste un moyen de trier.

C’est tout pour aujourd’hui, je reviens vers vous au plus vite !

[^1]: Tavan, Chloé. « École publique, école privée. Comparaison des trajectoires et de la réussite scolaires », *Revue française de sociologie*, vol. vol. 45, no. 1, 2004, pp. 133-165.
[^2]: Frouillou Leïla, Pin Clément, van Zanten Agnès, « Les plateformes APB et Parcoursup au service de l’égalité des chances ? L’évolution des procédures et des normes d’accès à l’enseignement supérieur en France », *L'Année sociologique*, 2020/2 (Vol. 70), p. 337-363.
[^3]: van Zanten, Agnès. « La fabrication familiale et scolaire des élites et les voies de mobilité ascendante en France », *L'Année sociologique*, vol. vol. 66, no. 1, 2016, pp. 81-114.
