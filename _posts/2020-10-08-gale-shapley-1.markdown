---
layout: post
title:  "Gale-Shapley (1/2) : quel algorithme pour l'admission dans le supérieur ?"
date:   2020-10-08 12:00:00 +0100
---
Bonjour tout le monde ! Voici mon premier « vrai » article où on va parler de concret. Vous êtes déjà très nombreux·ses à avoir répondu à mon sondage, et j'ai déjà pu faire un entretien (d'autres sont à venir). Votre implication me fait chaud au cœur et je vous promet des résultats sur ces points très prochainement.

Mais aujourd'hui, ce n'est pas vraiment le sujet. Notre approche sera même relativement déshumanisée puisqu'il va s'agir de parler du problème mathématique qui se pose lorsque l'on doit affecter les élèves à des formations, en tenant compte de leurs préférences et de celles des formations. Nous verrons les élèves comme des acteurs·ices parfaitement rationnels, des listes de vœux hiérarchisées accompagnée de résultats scolaires somme toute. Mais ce n'est pas un mal pour autant : cette approche est nécessaire pour comprendre bien des choses concernant le fonctionnement intrinsèque de la plateforme. Alors, accrochez-vous bien, il y a beaucoup à comprendre ! Comme il y a beaucoup à dire, je me contenterai ici d'introduire le problème qui se pose et de présenter l'algorithme de Gale-Shapley, utilisé par APB et reproduit par Parcoursup. La comparaison de l'implantation de l'algorithme entre les deux plateformes fera l'objet d'un second article.

# Le problème des mariages stables

On parle souvent des injustices du système Parcoursup, de témoignages concernant des situations qui paraissent injustes : pourquoi un tel a-t-il obtenu telle formation et moi celle-ci alors que nous aurions tous deux préféré l'autre formation ? Pour ça, introduisons deux définitions :

- Une situation est dite **stable** (ou « juste ») si personne ne se trouve en situation de jalousie justifiée, c'est-à-dire une situation où `A` n'obtient pas une formation que `B` a obtenu, alors que `A` était mieux classé que `B` par la formation.
- Une situation est dite **Pareto-efficace** du point de vue des élèves si la situation décrite plus haut (`A` obtient la formation `C` et `B` obtient `D` alors que `A` préférait `D` à `C` et `B` préférait `C` à `D`) ne se produit pas.

Pour que ces deux définitions aient un sens, il faut que les candidat·e·s aient des préférences concernant les formations, c'est-à-dire puissent produire un classement hiérarchisé de leurs vœux (c'est *a priori* le cas, que ce classement soit demandé dans la plateforme comme dans APB ou non comme dans Parcoursup). De même, il faut supposer que les formations puissent formuler des préférences concernant les élèves (en pratique les classer selon leurs résultats scolaires et éventuellement d'autres éléments du dossier). Cette deuxième hypothèse semble raisonnable, mais elle demande des moyens importants aux formations. J'ai pu en effet entendre le témoignage d'une personne chargée du tri des dossiers dans une université, qui m'expliquait que celui-ci était relativement bâclé faute de moyens suffisants pour le faire. Mais passons, la question des classements fera l'objet de futurs articles.

Un constat semble s'imposer : les propriétés de stabilité et de Pareto-efficacité paraissent toutes les deux importantes. Lors de l'entretien que j'ai réalisé avec un élève de CPGE, je lui ai présenté les deux situations que ces propriétés permettent d'éviter, et elles lui semblaient évidemment anormales. Sauf que voilà : **il existe des situations où stabilité et Pareto-efficacité  sont incompatibles**. C'est un résultat mathématique, que je peux facilement démontrer par une simulation en Python. En effet, comme il s'agit d'une assertion d'existence, exhiber un exemple suffit, et un programme nous épargnera de douloureux calculs ! On peut représenter les préférences par des listes :

{% highlight python %}
# Bien sûr, les données mises ici ne sont qu'un exemple !
formations = {'Université', 'CPGE', 'DUT'}
etudiants = {'Arnaud', 'Élodie', 'Sacha'}
preferences_etudiants = {
    'Arnaud': ['Université', 'CPGE', 'DUT'],  # Arnaud préfère l'université à la CPGE, et la CPGE au DUT.
    'Élodie': ['CPGE', 'Université', 'DUT'],
    'Sacha': ['Université', 'CPGE', 'DUT'],
}
preferences_formations = {
    'Université': ['Élodie', 'Arnaud', 'Sacha'],
    'CPGE': ['Sacha', 'Élodie', 'Arnaud'],
    'DUT': ['Arnaud', 'Élodie', 'Sacha']
}


# On code quelques fonctions qui nous seront utiles plus tard.
def etudiant_prefere(preference_etudiant, formation_a, formation_b):
    return preference_etudiant.index(formation_a) < preference_etudiant.index(formation_b)


def formation_prefere(preference_formation, etudiant_a, etudiant_b):
    return preference_formation.index(etudiant_a) < preference_formation.index(etudiant_b)
{% endhighlight %}

À partir de cette modélisation, on code des fonctions permettant de déterminer si une situation est stable et/ou efficace (et on les teste !) :

{% highlight python %}
# Une allocation sera représentée de la façon suivante :
# allocation = {'Arnaud': 'Université', 'Élodie': 'CPGE', 'Sacha': 'DUT'}
def est_stable(allocation, preferences_etudiants, preferences_formations):
    """Renvoie un booléen disant si l'allocation passée en entrée est stable."""

    for i in etudiants:
        for j in {e for e in etudiants if e != i}:
            # Tous les couples d'étudiants distincts passent ce test.
            if formation_prefere(preferences_formations[allocation[j]], i, j) and etudiant_prefere(preferences_etudiants[i], allocation[j], allocation[i]):
                return False
    return True


def est_efficace(allocation, preferences_etudiants):
    """Renvoie un booléen disant si l'allocation passée en entrée est efficace."""

    for i in etudiants:
        for j in {e for e in etudiants if e != i}:
            if etudiant_prefere(preferences_etudiants[i], allocation[j], allocation[i]) and etudiant_prefere(preferences_etudiants[j], allocation[i], allocation[j]):
                return False
    return True


# Quelques tests pour vérifier que les fonctions sont codées correctement.
assert etudiant_prefere(preferences_etudiants['Élodie'], 'CPGE', 'DUT')
assert not etudiant_prefere(preferences_etudiants['Élodie'], 'DUT', 'Université')
assert est_stable({'Arnaud': 'DUT', 'Élodie': 'Université', 'Sacha': 'CPGE'}, preferences_etudiants, preferences_formations)
assert not est_stable({'Arnaud': 'DUT', 'Élodie': 'CPGE', 'Sacha': 'Université'}, preferences_etudiants, preferences_formations)
assert est_efficace({'Arnaud': 'Université', 'Élodie': 'CPGE', 'Sacha': 'DUT'}, preferences_etudiants)
assert not est_efficace({'Arnaud': 'DUT', 'Élodie': 'Université', 'Sacha': 'CPGE'}, preferences_etudiants)
{% endhighlight %}

Il ne reste plus qu'à faire un petit code testant les 6 (i.e. la factorielle du nombre d'individus et de places, 3 dans notre exemple) combinaisons possibles et indiquant s'il en existe une étant à la fois stable et efficace :

{% highlight python %}
# On fait une compréhension de liste
allocations = [{'Arnaud': f1, 'Élodie': f2, 'Sacha': f3} for f1 in formations for f2 in formations if f1 != f2 for f3 in formations if f3 not in (f1, f2)]
# On va afficher pour chaque allocation V si le test est vrai, F s'il est faux.
print('N° | Stable | Efficace')
for i in range(len(allocations)):
    print('{0}  | {1}      | {2}'.format(i + 1, 'V' if est_stable(allocations[i], preferences_etudiants, preferences_formations) else 'F', 'V' if est_efficace(allocations[i], preferences_etudiants) else 'F'))
{% endhighlight %}

Et on exécute le tout. Le résultat obtenu est :

{% highlight text %}
N° | Stable | Efficace
1  | F      | V
2  | F      | V
3  | F      | F
4  | F      | V
5  | V      | F
6  | F      | V
{% endhighlight %}

On constate qu'aucune des allocations (nous les avons toutes testées !) n'est à la fois efficace et stable. La conclusion est sans appel : quoi que l'on fasse, il va nous falloir sacrifier en partie l'une de ces propriétés. La personne que j'ai interrogée préférait la stabilité, car les situations de jalousie justifiée lui semblaient particulièrement anormale. Ça tombe bien, c'est aussi mon avis et celui du gouvernement, qui **a retenu la propriété de stabilité pour APB et Parcoursup**. Nous sommes d'accord sur un point : c'est déjà ça.

Maintenant se pose la question suivante : comment peut-on, compte tenu des préférences des formations comme de celles des candidat·e·s, aboutir à une situation stable ? En fait, c'est loin d'être un problème nouveau : il est connu en algorithmique sous le nom de **problème des mariages stables** (il est usuellement présenté comme un problème consistant à construire des couples stables d'un homme et d'une femme).

# L'algorithme de Gale-Shapley

L'algorithme généralement utilisé pour résoudre ce problème date de 1956. Il fut proposé dans [un article](http://www.u.arizona.edu/~mwalker/501BReadings/Gale&Shapley_AMM1962.pdf) par les mathématiciens David Gale et Lloyd Shapley. Non seulement leur algorithme donne une solution stable, mais elle est en plus « aussi Pareto-efficace que possible », c'est-à-dire qu'elle est efficace parmi les solutions stables (il n'y aucune situation où `A` et `B` préféreraient échanger leur formation **et** où ce changement mènerait encore à une solution stable).

Par ailleurs, cet algorithme se distingue aussi par sa simplicité. Concrètement, voici comment les choses se passent (pour simplifier les choses, je présente l'algorithme comme si chaque formation avait une seule place, mais l'adaptation est assez triviale, il suffit de voir les différentes places d'une même formation comme plusieurs formations partageant la même liste de préférence et étant également classées par les candidat·e·s) :

1. Au début, aucune affectation n'a lieu, mais tous les candidat·e·s ont une liste ordonnée de préférences des formations, et réciproquement.
2. Les candidat·e·s proposent à leur premier vœu (la formation tout en haut de leur liste) de les accepter.
3. Les formations, pour celles qui ont reçu des propositions, décident d'admettre la personne ayant postulé qui se situe le plus en haut de leur liste. Mais cette admission est *temporaire*, elles se réservent le droit de l'annuler si on leur envoie une candidature qu'elles préfèrent.
4. Les candiat·e·s qui se retrouvent sur le carreau envoient maintenant une candidature au vœu qui suit dans leur liste.
5. De même, les formations admettent maintenant leur candidat·e préféré·e parmi les candidatures reçues et l'éventuelle personne déjà admise. Si cela est nécessaire, cette personne verra donc son admission finalement révoquée.
6. Les étapes 4 et 5 se poursuivent jusqu'à ce qu'il ne reste plus de place vacante ou d'élève sans affectation, et on obtient alors l'allocation finale.

Ensuite, il est relativement rapide : s'il y a `n` places de formations et `n` candidat·e·s (il peut y avoir plus de l'un ou de l'autre mais dans ce cas, soit des élèves se retrouvent sur le tapis, soit des places restent vacantes), la boucle de l'algorithme s'exécute au pire `n × n` fois.

Enfin, il y a une dernière propriété particulièrement intéressante : l'algorithme est **non manipulable**, c'est-à-dire qu'aucun·e acteur·ice (qu'il s'agisse d'un·e élève ou d'une formation) n'a intérêt à mentir sur ses préférences réelles. En pratique, cela signifie qu'il n'est pas nécessaire de chercher à établir une « stratégie » dans la hiérarchisation de ses vœux puisque la meilleure d'entre elles est justement d'être honnête.

Le fait qu'un algorithme aussi simple possède d'aussi belles propriétés est en soi assez époustouflant. Évidemment, on passera les démonstrations, mais n'hésitez pas à aller les lire dans l'article (elles sont loin d'être aussi complexes qu'elles peuvent en avoir l'air) !

Vous l'avez deviné, je ne vous parle pas de cet algorithme par hasard. Il s'agit en effet de celui qui était implanté dans APB. Mais la théorie fonctionnait-elle aussi bien que souhaité ? Et dans Parcoursup, qu'en est-il ? Ce sera l'objet de mon prochain article !
