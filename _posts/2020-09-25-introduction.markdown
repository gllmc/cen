---
layout: post
title:  "Bienvenue sur mon blog !"
date:   2020-09-25 12:00:00 +0100
---


Je vous souhaite la bienvenue sur ce site ! Ceci est le premier billet de mon blog, donc je vais essayer de mettre les choses au clair sur ses objectifs
et ce dont je parlerai dessus. Pour commencer, laissez-moi me présenter : je m'appelle Parydro (sur ce blog du moins...) et je suis lycéenne en terminale. Je m'apprête donc à passer la procédure Parcoursup. Par ailleurs, je suis aussi très engagée : élue au CVL de mon lycée, j'essaie de me battre pour que nos droits soient respectés. Comme je suis aussi férue d'informatique, la question de la plateforme est naturellement venue à moi.

# Parcoursup, c'est quoi ?

Si tout le monde a au moins une idée de la question, il convient de clarifier un peu le rôle de cette plateforme, tout du moins d'un point de vue historique.

{:refdef: style="text-align: center;"}
![Logo de Parcoursup]({{ "/assets/logo_parcoursup.png" | relative_url }})
{: refdef}

Parcoursup est la plateforme numérique chargée de **l'affectation des néo-bacheliers·ères dans les formations du supérieur**. En effet, près d'un million de jeunes obtiennent leur bac chez année, et attribuer à chacun d'eux·elles une formation qui lui convienne au mieux est loin d'être un problème trivial. En pratique, nous autres lycéen·ne·s formulons des vœux vers le mois de février, auprès des formations qui nous intéressent. Celles-ci étudient ensuite notre dossier, puis nous recevons des réponses à partir de mai. Celles-ci peuvent être de différents types :

- **Oui** : notre dossier est retenu.
- **Oui si** : notre dossier est retenu, mais à la condition de suivre une année de remise à niveau ou des cours supplémentaires.
- **Non** : seules les formations dites « sélectives » peuvent émettre une telle réponse (CPGE, DUT, etc.). En pratique, cela signifie que les licences simples à l'université ne peuvent techniquement pas refuser un dossier.
- **En attente (si)** : théoriquement, nous pourrions être dans l'une des deux premières situations, mais il n'y a pas assez de place. Nous sommes donc sur liste d'attente.

Dans les faits, la dernière de ces réponses est extrêmement fréquente, surtout si l'on a postulé principalement à des formations dites « non sélectives ». Dans ce cas, il faut attendre que les personnes ayant reçu plusieurs réponses positives fassent leur choix, libérant ainsi des places. Notre dossier s'actualise tous les matins et la phase d'admission s'étend ainsi sur de longs mois, avec une pause pendant le bac. Mais évidemment, toutes les personnes en attente n'obtiennent pas de place, que la formation soit sélective ou non. Une procédure complémentaire est mise en route pour celles et ceux qui ne trouvent toujours pas de place, mais elle ne s'achèvent qu'en septembre et ne donne bien entendu accès qu'aux établissement où il reste de la place (donc les moins côtés).

# Pourquoi de tels débats sur la plateforme ?

L'une des questions qui vient tout de suite est : que se passe-t-il entre février et mars ? Qui étudie notre dossier et décide de notre sort : des humains ou des algorithmes ? Avec quelles conséquences ? La réponse est évidemment loin d'être purement binaire et elle a d'ailleurs fait maintes fois polémique dans le débat public. En février 2020, dans une [tribune au *Figaro*](https://www.lefigaro.fr/vox/societe/parcoursup-quand-l-avenir-de-vos-enfants-se-joue-a-la-roulette-russe-20200522), l'UNI (un syndicat étudiant qui se réclame de la droite) expliquait que « l'avenir de [nos] enfants se joue à la roulette russe ». Deux mois plus tôt, la Cour des Comptes parlait de « paramètres parfois contestables » de sélection. Pour autant, le processus est loin de ne faire que des mécontent·e·s. En particulier, il élimine totalement le tirage de sort, à l'origine d'une forte polémique ayant provoqué à elle-seule le changement de plateforme.

Ensuite, on peut s'interroger sur la phase d'admission en elle-même. Pourquoi dure-t-elle aussi longtemps et est-il impossible de faire autrement ? Est-elle génératrice d'anxiété chez les lycéen·ne·s ? Sommes-nous tous égaux·ales face à cette attente ou y a-t-il des divergences selon le milieu social ?

J'ai décidé de mener l'enquête pour en savoir plus et tenter, dans la mesure du possible, de faire la part des choses et d'identifier les critiques fondées que l'on peut faire sur Parcoursup. Le but de ce blog sera de vous partager tous les résultats que j'obtiendrai. Mais avant de terminer ce billet, j'aimerais vous proposer un petit retour en arrière des procédures d'admission dans le supérieur :

- Historiquement, il n'y avait pas de procédure centralisée. Chacun·e envoyait son dossier aux formations qui l'intéressaient, qui pouvaient (ou non dans le cas d'une formation sélective) l'accepter. Dans les faits, ce fonctionnement, outre sa très grande complexité administrative, n'était pas du tout optimal : il aboutissait à de nombreuses injustices et une procédure centralisée s'imposait.
- En 2002, le portail APB (pour « Admission Post-Bac ») est créé pour les classes préparatoires. L'idée est en soi assez révolutionnaire, car un portail unique permet une très grande simplification administrative. Mais celle-ci peut aussi provoquer de plus grandes difficultés pour comprendre le fonctionnement du système (sur lequel nous reviendrons, je ne vous divulgâche pas !).
- En 2007, APB est généralisé à toutes les formations. Contrairement à Parcoursup, APB demandait aux candidat·e·s de produire un classement hiérarchisé de leurs vœux. Lors de la phase d'admission, l'on savait directement où l'on était pris·e au lieu de recevoir des résultats vœu par vœu. Par ailleurs, la liste d'attente dans les formations non sélectives ne dépendait pas des résultats scolaires, mais de la position du vœu dans la liste, de la proximité géographique, et éventuellement d'un tirage au sort si les critères précédents ne suffisent pas (manque de places).
- En 2017, ce tirage au sort fait fortement polémique, conduisant à la mise en place de Parcoursup en 2018, par le biais de la très controversée loi ORE (orientation et réussite des étudiants).

Ce rappel étant fait, j'aimerai vous demander une dernière chose. Évidemment, je vais devoir m'informer énormément et faire de nombreuses recherches sur le sujet, et je n'y manquerai pas ! Cependant, je pense que j'ai aussi besoin de *votre* ressenti pour aboutir à des conclusions qui prennent en compte les enjeux sociaux derrière la plateforme : vous qui êtes déjà passé par Parcoursup ou vous apprêtez à entamer la procédure, comment le vivez-vous/l'avez-vous vécu ? Est-ce une source d'anxiété ?

Je vous invite donc à compléter [ce Google Form](https://docs.google.com/forms/d/e/1FAIpQLSecMvvDDM0xs-iduyNR6lN5p0DYTmmcOg8I3qfIY9RHpUCAgQ/viewform?usp=sf_link), quel que soit votre rapport à Parcoursup. Par ailleurs, si certain·e·s sont disponibles pour un entretien sociologique, n'hésitez pas à me contacter !

Merci de m'avoir suivie et à bientôt !

------------

**[Edit du 6 octobre]** Merci beaucoup d'avoir été si nombreux·ses à répondre à mon sondage ! Vous avez été 188 terminales et bacheliers·ères à participer ! J'ai pu avoir des entretiens poussés avec deux d'entre vous, donc merci beaucoup à E. et A. pour leurs éclaircissements ! E. est passé par Parcoursup en 2018 et vient du lycée Albert Camus (lycée public de Bois Colombes), tandis que A. va passer par Parcoursup pour la première fois cette année et est scolarisée en terminale au lycée Galilée (à Combs la Ville).
