---
layout: page
title: À propos
permalink: /a-propos/
---

**Ce blog n'est pas celui d'une vraie personne.** Nous sommes cinq étudiant·e·s en deuxième année d'une double licence de sciences et de sciences sociales et effectuons une exploration numérique sur le thème de Parcoursup. Son objectif est de souligner la multiplicité des enjeux derrière la formule, qu'ils soient numériques (accessibilité et algorithmes du site) ou sociaux (lien avec l'anxiété scolaire), et nous avons en cela choisi d'axer notre enquête sur la question des inégalités.

Ce blog est celui d'une lycéenne fictive, Parydro (il s'agit d'un pseudonyme), particulièrement intéressée par l'informatique et la programmation. En désaccord avec nombre des décisions prises sur le fonctionnement de la plateforme, elle poste des articles expliquant ce qui lui semble problématique. Ceux-ci sont agrémentés de chiffres, de données d'entretiens sociologiques et de recherches documentaires que nous avons nous-mêmes réalisés. Bien évidemment, le fonctionnement de ce blog fait que les textes qu'il contient adoptent nécessairement un ton militant. Mais cela ne nous empêche aucunement de faire preuve de nuance et de rigueur méthodologique dans nos analyses : les sources utilisées seront précisées clairement à chaque fois, de même que les potentielles limites des approches considérées.

{% if site.content_license and site.license_link %}Le contenu de notre enquête (donc de ce blog) est sous licence [{{ site.content_license }}]({{ site.license_link }}). {% if site.content_license == 'CC-BY-NC-SA' %}Cela signifie que vous pouvez reprendre notre travail pour un usage non commercial, mais en nous citant et en partageant votre contenu dans les mêmes conditions. Vous pouvez pour cela utiliser le pseudo Parydro et communiquer l'adresse de ce blog.{% endif %}{% endif %}

Le site est codé à l'aide de [Jekyll](https://jekyllrb.com/) et est hébergé par [GitHub Pages](https://pages.github.com/). {% if site.source_code %}Son [code source]({{ site.source_code }}) est public.{% endif %}
